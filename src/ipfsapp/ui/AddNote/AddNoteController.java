/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.ui.AddNote;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextArea;
import com.jfoenix.controls.JFXTextField;
import ipfsapp.DaemonManager.IPFSHandler;
import ipfsapp.Model.IPFSFile;
import ipfsapp.ui.listMyFiles.MyFileList;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author CraigDev
 */
public class AddNoteController implements Initializable {

    @FXML
    private StackPane rootPane;
    @FXML
    private AnchorPane mainContainer;
    @FXML
    private JFXTabPane mainTabPane;
    @FXML
    private JFXTextField noteTitle;
    @FXML
    private JFXTextArea noteContent;

    private IPFSHandler ipfsHandler;

    private Boolean isEditMode = false;

    private String fileId = null;
    @FXML
    private JFXButton saveNote;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ipfsHandler = new IPFSHandler();
        mainTabPane.tabMinWidthProperty().bind(rootPane.widthProperty().divide(mainTabPane.getTabs().size()).subtract(20));
    }

    @FXML
    private void handleSaveNote(ActionEvent event) {
        if (isEditMode) {
            saveEditedIPFS();
        } else {
            addNoteToIPFS();
        }
    }

    public void inflateUI(MyFileList content, Boolean isMyFile) {
        isEditMode = Boolean.TRUE;
        fileId = content.getId();
        noteTitle.setText(content.getName().replace(".txt", ""));
        noteTitle.setEditable(false);
        noteContent.setEditable(isMyFile);
        saveNote.setDisable(!isMyFile);

        loadNoteContent(content.getHash());
    }

    private void addNoteToIPFS() {
        Task<Optional<IPFSFile>> task = new Task<Optional<IPFSFile>>() {
            @Override
            protected Optional<IPFSFile> call() throws Exception {
                return ipfsHandler.addNote(noteTitle.getText() + ".txt", noteContent.getText());
            }
        };

        task.setOnSucceeded((e) -> {
            Optional<IPFSFile> ipfsFile = task.getValue();

            if (ipfsFile.isPresent()) {
                JFXButton okay = new JFXButton("Okay");
                okay.setOnAction((btn) -> {
                    IPFSUtil.closeStage(mainContainer);
                });
                AlertMaker.showMaterialDialog(rootPane, mainContainer, Arrays.asList(okay), "Note Added", ipfsFile.get().getFileName() + " Successfully Added To IPFS");
            } else {
                AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Failed", "An Error Occured, Note Could Not Be Added");
            }
        });

        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

    private void saveEditedIPFS() {
        Task<Boolean> task = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                return ipfsHandler.saveEditedContent(fileId, noteTitle.getText() + ".txt", noteContent.getText());
            }
        };

        task.setOnSucceeded((e) -> {
            if (task.getValue()) {
                JFXButton okay = new JFXButton("Okay");
                okay.setOnAction((btn) -> {
                    IPFSUtil.closeStage(mainContainer);
                });
                AlertMaker.showMaterialDialog(rootPane, mainContainer, Arrays.asList(okay), "Note Added", noteTitle.getText() + ".txt" + " Successfully Saved Note To IPFS");
            } else {
                AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Failed", "An Error Occured, Note Could Not Be Saved");
            }
        });

        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

    private void loadNoteContent(String hash) {
        Task<Optional<String>> task = new Task<Optional<String>>() {
            @Override
            protected Optional<String> call() throws Exception {
                ipfsHandler.pinBlock(hash);
                return ipfsHandler.getHashConent(hash);
            }
        };

        task.setOnSucceeded((e) -> {
            Optional<String> content = task.getValue();
            if (content.isPresent()) {
                noteContent.setText(content.get());
            } else {
                AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Error", "Could Not Load Note, an error occured");
            }

        });

        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

}
