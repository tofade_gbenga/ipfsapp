package ipfsapp.ui.SignIn;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;
import ipfsapp.DaemonManager.DaemonManager;
import ipfsapp.DataHandler.DataDAO;
import ipfsapp.DataHandler.DatabaseHandler;
import ipfsapp.Model.User;
import ipfsapp.Preferences.AppPreference;
import ipfsapp.SocketSync.SocketClient;
import ipfsapp.SocketSync.SocketServer;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import ipfsapp.wizard.SetupWizard;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
//import org.apache.commons.codec.digest.DigestUtils;

public class LoginController implements Initializable {

    @FXML
    private JFXTextField username;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXSpinner loader;

    private DatabaseHandler handler;

    @FXML
    private StackPane rootPane;
    @FXML
    private AnchorPane mainContainer;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void handleLoginButtonAction(ActionEvent event) {
        loader.setVisible(true);
        String uname = username.getText();
        String pword = password.getText();
        doLogin(uname, pword);
    }

    void loadMain() {

        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/MainView/MainView.fxml"), "Dashboard", (Stage) username.getScene().getWindow());

    }

    @FXML
    private void handleLoadSignUp(ActionEvent event) {
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/AddUser/User_add.fxml"), "Add User", (Stage) username.getScene().getWindow());
    }

    private void doLogin(String uname, String pword) {
        Task<User> task = new Task<User>() {
            @Override
            protected User call() throws Exception {
                return DataDAO.getUserByUserName(uname);
            }
        };

        task.setOnSucceeded((e) -> {
            loader.setVisible(false);
            User user = task.getValue();
            if (user != null) {
                if (user.getPassword().equalsIgnoreCase(pword)) {
                    JFXButton btn = new JFXButton("Okay!");
                    btn.setOnAction((event) -> {
                        getActivationStatus();
//                        IPFSUtil.closeStage(username);
                    });
                    AppPreference.setUser(user);
                    AlertMaker.showMaterialDialog(rootPane, mainContainer, Arrays.asList(btn), "Success", user.getName() + " Successfully Logged In");
                    // Initiate Socket
//                    SocketServer socThread = new SocketServer();
//                    SocketClient socketClient = new SocketClient();
//
//                    IPFSUtil.SOCKET_THREAD_POOL.submit(socThread);
//                    IPFSUtil.SOCKET_THREAD_POOL.submit(socketClient);
                } else {
                    AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Error", "Incorrect Password");
                    password.getStyleClass().add("wrong-credentials");
                }

            } else {
                username.getStyleClass().add("wrong-credentials");
                password.getStyleClass().add("wrong-credentials");
                AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Error", uname + " Not Found");

            }
        });
        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

    /**
     * get activation status of ipfs
     */
    private void getActivationStatus() {
        Task<Boolean> task = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                return DataDAO.isGenBlockActivated();
            }
        };

        task.setOnSucceeded((e) -> {
            Boolean isActivated = task.getValue();
            if (isActivated) {
                if (!DaemonManager.isDaemonRunning()) {
                    DaemonManager manager = new DaemonManager();
                }
                //Load Main Page
                loadMain();

                //Rebroadcast name space that holds all users info
                IPFSUtil.reBroadcastNameSpace();
                IPFSUtil.setAllBlocks();
                IPFSUtil.publishMyBlocks();
            } else {
                loadSetupWizard();
            }
        });

        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

    /**
     * load Setup wizard
     */
    private void loadSetupWizard() {
        SetupWizard wizard = new SetupWizard();
        wizard.doInit();
    }

}
