package ipfsapp.ui.AddUser;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXSpinner;
import com.jfoenix.controls.JFXTextField;
import ipfsapp.DataHandler.DataDAO;
import ipfsapp.DataHandler.DatabaseHandler;
import ipfsapp.Model.User;
import ipfsapp.Preferences.AppPreference;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.net.URL;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class UserAddController implements Initializable {

    DatabaseHandler handler;

    @FXML
    private JFXTextField name;
    @FXML
    private JFXButton saveButton;
    @FXML
    private JFXButton cancelButton;

    private Boolean isInEditMode = false;
    @FXML
    private StackPane rootPane;
    @FXML
    private AnchorPane mainContainer;
    @FXML
    private JFXPasswordField password;
    @FXML
    private JFXTextField username;
    @FXML
    private JFXSpinner loader;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        handler = DatabaseHandler.getInstance();
    }

    @FXML
    private void cancel(ActionEvent event) {
        Stage stage = (Stage) name.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void addMember(ActionEvent event) {
        String mName = name.getText();
        String mUsername = username.getText();
        String mPassword = password.getText();

        Boolean flag = mName.isEmpty() || mUsername.isEmpty() || mPassword.isEmpty();
        if (flag) {
            AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Insufficient Data", "Please enter data in all fields.");
            return;
        }

        loader.setVisible(true);
        checkExistingUserTask(mName, mUsername, mPassword);

    }

    public void infalteUI(User member) {
        name.setText(member.getName());
        password.setText(member.getPassword());
        username.setText(member.getUsername());

        isInEditMode = Boolean.TRUE;
    }

    private void clearEntries() {
        name.clear();
        username.clear();
        password.clear();
    }

    private void checkExistingUserTask(String mName, String mUsername, String mPassword) {
        Task<Boolean> checkUserTask = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                return DataDAO.isUserExist(mUsername);
            }
        };

        checkUserTask.setOnSucceeded((e) -> {
            Boolean isExist = checkUserTask.getValue();
            if (!isExist) {
                addOrUpdateUserTask(mName, mUsername, mPassword);
            } else {
                loader.setVisible(false);
                AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Duplicate Username", "Member with same username exists.\nPlease use new Username!!!");
            }
        });

        IPFSUtil.TASK_EXECUTOR.execute(checkUserTask);
    }

    private void addOrUpdateUserTask(String mName, String mUsername, String mPassword) {
        Task<Boolean> doAddMember = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {

                if (isInEditMode) {
                    return handleUpdateMember();
                } else {
                    User member = new User(mUsername, mPassword, mName);
                    return DataDAO.insertNewUser(member);
                }

            }
        };

        doAddMember.setOnSucceeded((e) -> {
            Boolean isUpdatedOrAdded = doAddMember.getValue();
            loader.setVisible(false);
            if (isInEditMode) {
                if (isUpdatedOrAdded) {
                    AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Success", "User data updated.");
                } else {
                    AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Failed", "Cant update user.");
                }
            } else {
                if (isUpdatedOrAdded) {
                    JFXButton button = new JFXButton("Okay");
                    button.setOnAction(ea -> goToLogin());
                    AlertMaker.showMaterialDialog(rootPane, mainContainer, Arrays.asList(button), "New User added", mName + " has been added");
                    clearEntries();
                } else {
                    AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Failed to add new user", "Check you entries and try again.");
                }
            }
        });

        IPFSUtil.TASK_EXECUTOR.execute(doAddMember);
    }

    private Boolean handleUpdateMember() {
        User user = AppPreference.getUser();
        user.setUsername(username.getText());
        user.setPassword(password.getText());
        user.setName(name.getText());
        user.setDateModified(Timestamp.valueOf(LocalDateTime.now()));
        return DatabaseHandler.getInstance().updateUser(user);
    }

    private void goToLogin() {
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/SignIn/login.fxml"), "IPFS Login", (Stage) username.getScene().getWindow());

    }
}
