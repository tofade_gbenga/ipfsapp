/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.ui.peers;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTabPane;
import io.ipfs.api.Peer;
import ipfsapp.DaemonManager.IPFSHandler;
import ipfsapp.utils.IPFSUtil;
import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;

/**
 * FXML Controller class
 *
 * @author CraigDev
 */
public class PeersController implements Initializable {

    @FXML
    private StackPane rootPane;
    @FXML
    private TableView<IPFSPeers> tableView;
    @FXML
    private TableColumn<IPFSPeers, String> addressCol;
    @FXML
    private TableColumn<IPFSPeers, String> hashCol;
    @FXML
    private TableColumn<IPFSPeers, String> ownerCol;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private AnchorPane mainContainer;
    @FXML
    private JFXTabPane mainTabPane;
    @FXML
    private JFXHamburger hamburger;

    private IPFSUtil iPFSUtil;
    @FXML
    private TableColumn<IPFSPeers, String> snCol;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        iPFSUtil = new IPFSUtil();
        iPFSUtil.initDrawer(hamburger, drawer, PeersController.class);
        mainTabPane.tabMinWidthProperty().bind(rootPane.widthProperty().divide(mainTabPane.getTabs().size()).subtract(20));
        initializeTableCol();
        loadData();
    }

    private void initializeTableCol() {
        snCol.setCellValueFactory(new PropertyValueFactory<>("sn"));
        addressCol.setCellValueFactory(new PropertyValueFactory<>("address"));
        hashCol.setCellValueFactory(new PropertyValueFactory<>("hash"));
        ownerCol.setCellValueFactory(new PropertyValueFactory<>("owner"));
    }

    @FXML
    private void handleRefresh(ActionEvent event) {
        loadData();
    }

    private void loadData() {
        Task<Optional<List<Peer>>> task = new Task<Optional<List<Peer>>>() {
            @Override
            protected Optional<List<Peer>> call() throws Exception {
                IPFSHandler ipfsHandler = new IPFSHandler();
                return ipfsHandler.getConnectedPeers();
            }
        };

        task.setOnSucceeded((e) -> {
            ObservableList<IPFSPeers> tablePeers = FXCollections.observableArrayList();
            Optional<List<Peer>> OptionalListOfPeer = task.getValue();
            if (OptionalListOfPeer.isPresent()) {
                OptionalListOfPeer.get().stream().forEach((peer) -> {
                    String sn = String.valueOf(OptionalListOfPeer.get().indexOf(peer) + 1);
                    String hostAddress = peer.address.getHost() + ":" + peer.address.getTCPPort();
                    String peerHash = peer.id.toString();
                    tablePeers.add(new IPFSPeers(sn, hostAddress, peerHash, ""));

                });
            }
            tableView.setItems(tablePeers);
        });
        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

}
