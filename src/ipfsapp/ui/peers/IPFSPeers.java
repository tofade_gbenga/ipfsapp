/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.ui.peers;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author CraigDev
 */
public class IPFSPeers {

    private final SimpleStringProperty sn;
    private final SimpleStringProperty address;
    private final SimpleStringProperty hash;
    private final SimpleStringProperty owner;

    public IPFSPeers(String sn, String address, String hash, String owner) {
        this.sn = new SimpleStringProperty(sn);
        this.address = new SimpleStringProperty(address);
        this.hash = new SimpleStringProperty(hash);
        this.owner = new SimpleStringProperty(owner);
    }

    public String getSn() {
        return sn.get();
    }

    public String getAddress() {
        return address.get();
    }

    public String getHash() {
        return hash.get();
    }

    public String getOwner() {
        return owner.get();
    }

}
