/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.ui.MainView;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import io.ipfs.api.KeyInfo;
import io.ipfs.multihash.Multihash;
import ipfsapp.DaemonManager.IPFSHandler;
import ipfsapp.DataHandler.DataDAO;
import ipfsapp.Preferences.FileStatus;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import ipfsapp.wizard.SetupWizard;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author CraigDev
 */
public class MainViewController implements Initializable {

    @FXML
    private StackPane rootPane;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private AnchorPane rootAnchorPane;
    @FXML
    private JFXTabPane mainTabPane;
    @FXML
    private HBox book_info;
    @FXML
    private HBox member_info;
    @FXML
    private JFXHamburger hamburger;
    @FXML
    private Tab IPFSDashboardTab;

    private IPFSUtil iPFSUtil;

    private IPFSHandler iPFSHandler;

//    private void handleButtonAction(ActionEvent event) throws IOException {
//        controller.stopDaemon();
//    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {

        iPFSHandler = new IPFSHandler();
        iPFSUtil = new IPFSUtil();
        iPFSUtil.initDrawer(hamburger, drawer, MainViewController.class);
        mainTabPane.tabMinWidthProperty().bind(rootPane.widthProperty().divide(mainTabPane.getTabs().size()).subtract(20));
//        doInit();
    }

    private Stage getStage() {
        return (Stage) hamburger.getScene().getWindow();
    }

    @FXML
    private void handleMenuSettings(ActionEvent event) {
    }

    @FXML
    private void handleMenuClose(ActionEvent event) {
    }

    @FXML
    private void handleMenuFullScreen(ActionEvent event) {
    }

    @FXML
    private void handleAboutMenu(ActionEvent event) {
    }

    @FXML
    private void addFileToIPFS(ActionEvent event) {
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/AddFile/file_add.fxml"), "Add Files", getStage());
    }

    @FXML
    private void loadMyIPFSFiles(ActionEvent event) {
        FileStatus.setIsMyFiles(Boolean.TRUE);
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/listMyFiles/listFiles.fxml"), "My IPFS Files", getStage());
    }

    @FXML
    private void addDirectoryToIPFS(ActionEvent event) {
        AlertMaker.showSimpleAlert("Sorry", "This Feature is yet to be implemented");
    }

    @FXML
    private void loadAll_IPFSFiles(ActionEvent event) {
        FileStatus.setIsMyFiles(Boolean.FALSE);
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/listMyFiles/listFiles.fxml"), "My IPFS Files", getStage());
    }

    @FXML
    private void loadPeers(ActionEvent event) {
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/peers/Peers.fxml"), "Connected Peers", getStage());
    }

    @FXML
    private void loadSettings(ActionEvent event) {
    }

    @FXML
    private void handleMenuAdd(ActionEvent event) {
    }

    @FXML
    private void handleMenuViewMyFiles(ActionEvent event) {
    }

    @FXML
    private void handleMenuViewBlockFiles(ActionEvent event) {
    }

    @FXML
    private void handleMenuViewAddDirecory(ActionEvent event) {
    }

    private void doInit() {
//            iPFSHandler.publishPeerID("PeerID-83fd7f041", "Qma8cKNMrjy8anLiicPAShzEr1wcRnPCjXbgs4fmhwLpEV");
//            iPFSHandler.setupInitPeerKey();
//            iPFSHandler.addUserToGenBlock();

    }

}
