package ipfsapp.ui.MainView.ToolBar;

import com.jfoenix.controls.JFXButton;
import ipfsapp.Preferences.FileStatus;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.stage.Stage;

public class ToolbarController implements Initializable {

    @FXML
    private JFXButton dashboardBtn;

//    private BookReturnCallback callback;
//
//    public void setBookReturnCallback(BookReturnCallback callback) {
//        this.callback = callback;
//    }
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    private Stage getStage() {
        return (Stage) dashboardBtn.getScene().getWindow();
    }

    @FXML
    private void loadSettings(ActionEvent event) {
        IPFSUtil.loadWindow(getClass().getResource("/library/assistant/settings/settings.fxml"), "Settings", null);
    }

    @FXML
    private void addFileToIPFS(ActionEvent event) {
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/AddFile/file_add.fxml"), "Add Files", getStage());
    }

    @FXML
    private void addDirectoryToIPFS(ActionEvent event) {
        AlertMaker.showSimpleAlert("Sorry", "This Feature is yet to be implemented");
    }

    @FXML
    private void loadMyIPFSFiles(ActionEvent event) {
        FileStatus.setIsMyFiles(Boolean.TRUE);
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/listMyFiles/listFiles.fxml"), "My IPFS Files", getStage());
    }

    @FXML
    private void loadAll_IPFSFiles(ActionEvent event) {
        FileStatus.setIsMyFiles(Boolean.FALSE);
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/listMyFiles/listFiles.fxml"), "My IPFS Files", getStage());
    }

    @FXML
    private void loadPeers(ActionEvent event) {
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/peers/Peers.fxml"), "Connected Peers", getStage());
    }

    @FXML
    private void toDashBoard(ActionEvent event) {
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/MainView/MainView.fxml"), "Dashboard", getStage());
    }

}
