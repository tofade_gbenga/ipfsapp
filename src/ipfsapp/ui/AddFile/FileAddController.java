package ipfsapp.ui.AddFile;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTabPane;
import ipfsapp.DaemonManager.DaemonManager;
import ipfsapp.DaemonManager.IPFSHandler;
import ipfsapp.DataHandler.DatabaseHandler;
import ipfsapp.Model.IPFSFile;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import io.ipfs.api.KeyInfo;
import ipfsapp.DataHandler.DataDAO;
import java.util.List;
import java.util.Optional;
import javafx.stage.FileChooser;

public class FileAddController implements Initializable {

    DatabaseHandler handler;

    private Boolean isInEditMode = false;
    @FXML
    private StackPane rootPane;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private JFXHamburger hamburger;
    @FXML
    private AnchorPane mainContainer;
    @FXML
    private JFXTabPane mainTabPane;
    @FXML
    private Tab IPFSDashboardTab;

    private IPFSHandler ipfsHandler;

    private IPFSUtil iPFSUtil;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ipfsHandler = new IPFSHandler();
        iPFSUtil = new IPFSUtil();
        iPFSUtil.initDrawer(hamburger, drawer, FileAddController.class);
        mainTabPane.tabMinWidthProperty().bind(rootPane.widthProperty().divide(mainTabPane.getTabs().size()).subtract(20));

    }

    @FXML
    private void selectSingleFIle(ActionEvent event) throws IOException {

        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Select Text File For IPFS");
        fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("Text File", "*"));
        File selectedFile = fileChooser.showOpenDialog(mainContainer.getScene().getWindow());

        if (selectedFile != null) {
            AlertMaker.showTrayMessage("Selected File", selectedFile.getName());
            uploadFile(selectedFile);
        } else {
            AlertMaker.showErrorMessage("Canceled", "File Selection Canceled");
        }

    }

    @FXML
    private void addNoteToIPFS(ActionEvent event) {
        IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/AddNote/AddNote.fxml"), "Write Note", null);
    }

    private void uploadFile(File file) {
        Task<Optional<IPFSFile>> task = new Task<Optional<IPFSFile>>() {
            @Override
            protected Optional<IPFSFile> call() throws Exception {
                return ipfsHandler.addFile(file);
            }
        };

        task.setOnSucceeded((e) -> {
            Optional<IPFSFile> ipfsFile = task.getValue();

            if (ipfsFile.isPresent()) {
                AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "File Added", ipfsFile.get().getFileName() + " Successfully Added To IPFS");
                if (IPFSUtil.connectivity()) {
                    rebrodcastHashes();
                } else {
                    AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "No Internet", ipfsFile.get().getFileName() + "Successfully Added To IPFS, Looks like your device is not connected to the internet, publishing will be done at some other time in the Background.");
                }
            } else {
                AlertMaker.showMaterialDialog(rootPane, mainContainer, new ArrayList<>(), "Failed", "An Error Occured, File Could Not Be Added");
            }
        });

        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

    private void rebrodcastHashes() {
        Task<Boolean> task = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                return ipfsHandler.rebroadcastHashes();
            }
        };

        task.setOnSucceeded(e -> {
        });

        IPFSUtil.TASK_EXECUTOR.execute(task);
    }
}
