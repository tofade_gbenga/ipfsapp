/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.ui.listMyFiles;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.controls.JFXTabPane;
import com.jfoenix.controls.JFXTextField;
import ipfsapp.DataHandler.DataDAO;
import ipfsapp.Preferences.AppPreference;
import ipfsapp.Preferences.FileStatus;
import ipfsapp.ui.AddNote.AddNoteController;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

/**
 * FXML Controller class
 *
 * @author CraigDev
 */
public class ListFileController implements Initializable {

    @FXML
    private StackPane rootPane;
    @FXML
    private TableView<MyFileList> tableView;
    @FXML
    private TableColumn<MyFileList, String> nameCol;
    @FXML
    private TableColumn<MyFileList, String> hashCol;
    @FXML
    private TableColumn<MyFileList, String> ownerCol;
    @FXML
    private JFXDrawer drawer;
    @FXML
    private AnchorPane mainContainer;
    @FXML
    private JFXTabPane mainTabPane;
    @FXML
    private JFXHamburger hamburger;

    private IPFSUtil iPFSUtil;
    @FXML
    private TableColumn<MyFileList, String> snCol;

    private ObservableList<MyFileList> tableData = FXCollections.observableArrayList();
    @FXML
    private JFXTextField searchField;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        iPFSUtil = new IPFSUtil();
        iPFSUtil.initDrawer(hamburger, drawer, ListFileController.class);
        mainTabPane.getTabs().get(0).setText(FileStatus.getIsMyFiles() ? "My IPFS Files" : "All IPFS Files");
        mainTabPane.tabMinWidthProperty().bind(rootPane.widthProperty().divide(mainTabPane.getTabs().size()).subtract(20));
        initializeTable();
        loadData();
    }

    private void initializeTable() {
        snCol.setCellValueFactory(new PropertyValueFactory<>("sn"));
        nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
        hashCol.setCellValueFactory(new PropertyValueFactory<>("hash"));
        ownerCol.setCellValueFactory(new PropertyValueFactory<>("owner"));

        FilteredList<MyFileList> filteredData = new FilteredList<>(tableData);

        searchField.textProperty().addListener((ObservableList, oldV, newV) -> {
            filteredData.setPredicate(fileInfo -> {
                if (newV == null || newV.isEmpty()) {
                    return true;
                }
                String lowerCaseFilter = newV.toLowerCase();

                if (fileInfo.getName().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (fileInfo.getHash().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                } else if (fileInfo.getOwner().toLowerCase().contains(lowerCaseFilter)) {
                    return true;
                }
                return false;
            });
        });

        SortedList<MyFileList> sortedData = new SortedList<>(filteredData);

        sortedData.comparatorProperty().bind(tableView.comparatorProperty());

        tableView.setItems(sortedData);
    }

    @FXML
    private void handleRefresh(ActionEvent event) {
        loadData();
    }

    @FXML
    private void handleEditOption(ActionEvent event) {
        Optional<MyFileList> selectedFile = Optional.of(tableView.getSelectionModel().getSelectedItem());
        if (!selectedFile.isPresent()) {
            AlertMaker.showMaterialDialog(rootPane, tableView, new ArrayList<>(), "No Content Selected!!!", "No Content Was Selected, Please Select A Note to Edit");
            return;
        }

        if (!AppPreference.getUser().getName().equalsIgnoreCase(selectedFile.get().getOwner())) {
            JFXButton gotoNoteButton = new JFXButton("Okay");
            gotoNoteButton.setOnAction((e) -> {
                gotoNote(selectedFile, false);
            });
            AlertMaker.showMaterialDialog(rootPane, tableView, Arrays.asList(gotoNoteButton), "Sorry", "You can only read but can't edit this content as it was not added by you.");

        } else {
            gotoNote(selectedFile, true);
        }

    }

    private void gotoNote(Optional<MyFileList> selectedFile, Boolean isMyFile) {
        try {

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ipfsapp/ui/AddNote/AddNote.fxml"));
            Parent parent = loader.load();

            AddNoteController controller = loader.getController();
            controller.inflateUI(selectedFile.get(), isMyFile);
            Stage stage = new Stage(StageStyle.DECORATED);
            stage.setTitle("Edit My IPFS Content");
            stage.setScene(new Scene(parent));
            IPFSUtil.setStageIcon(stage);
            stage.show();

            stage.setOnCloseRequest((e) -> {
                handleRefresh(new ActionEvent());
            });

        } catch (IOException ex) {
            Logger.getLogger(ListFileController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadData() {
        Task<List> task = new Task<List>() {
            @Override
            protected List call() throws Exception {
                return DataDAO.FileLists(FileStatus.getIsMyFiles());
            }
        };

        task.setOnSucceeded((e) -> {
            tableData.clear();
            tableData.addAll(task.getValue());
        });
        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

}
