/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.ui.listMyFiles;

import javafx.beans.property.SimpleStringProperty;

/**
 *
 * @author CraigDev
 */
public class MyFileList {

    private final SimpleStringProperty id;
    private final SimpleStringProperty sn;
    private final SimpleStringProperty name;
    private final SimpleStringProperty hash;
    private final SimpleStringProperty owner;

    public MyFileList(String id, String sn, String name, String hash, String owner) {
        this.id = new SimpleStringProperty(id);
        this.sn = new SimpleStringProperty(sn);
        this.name = new SimpleStringProperty(name);
        this.hash = new SimpleStringProperty(hash);
        this.owner = new SimpleStringProperty(owner);
    }

    public String getId() {
        return id.get();
    }

    public String getSn() {
        return sn.get();
    }

    public String getName() {
        return name.get();
    }

    public String getHash() {
        return hash.get();
    }

    public String getOwner() {
        return owner.get();
    }

}
