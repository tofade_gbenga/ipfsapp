/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.Preferences;

import ipfsapp.Model.User;
import java.util.UUID;

/**
 *
 * @author CraigDev
 */
public class AppPreference {

    private static User user;
    private static final String SOCKET_ID = UUID.randomUUID().toString().replace("-", "");

    public static String getSOCKET_ID() {
        return SOCKET_ID;
    }
    
    public static User getUser() {
        return user;
    }

    public static void setUser(User user) {
        AppPreference.user = user;
    }

}
