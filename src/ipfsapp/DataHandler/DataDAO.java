/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.DataHandler;

import ipfsapp.Model.GenesisBlock;
import ipfsapp.Model.IPFSFile;
import ipfsapp.Model.Peer;
import ipfsapp.Model.User;
import ipfsapp.Preferences.AppPreference;
import ipfsapp.ui.listMyFiles.MyFileList;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CraigDev
 */
public class DataDAO {

    public static boolean insertNewUser(User member) {
        try {
            PreparedStatement statement = DatabaseHandler.getInstance().getConnection().prepareStatement(
                    "INSERT INTO Users(id,name,username,password) VALUES(?,?,?,?)");
            statement.setString(1, member.getId());
            statement.setString(2, member.getName());
            statement.setString(3, member.getUsername());
            statement.setString(4, member.getPassword());
            return statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean insertSocketNewUser(User member) {
        try {
            PreparedStatement statement = DatabaseHandler.getInstance().getConnection().prepareStatement(
                    "INSERT INTO Users(id,name,username,password,datemodified) VALUES(?,?,?,?,?)");
            statement.setString(1, member.getId());
            statement.setString(2, member.getName());
            statement.setString(3, member.getUsername());
            statement.setString(4, member.getPassword());
            statement.setTimestamp(5, member.getDateModified());

            return statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean isUserExist(String mUsername) {
        try {
            String checkstmt = "SELECT COUNT(*) FROM Users WHERE username=?";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(checkstmt);
            stmt.setString(1, mUsername);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int count = rs.getInt(1);
                System.out.println(count);
                return (count > 0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean isUserExistById(String id) {
        try {
            String checkstmt = "SELECT COUNT(*) FROM Users WHERE id=?";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(checkstmt);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int count = rs.getInt(1);
                System.out.println(count);
                return (count > 0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static User getUserByUserName(String mUsername) {
        try {
            String query = "SELECT * FROM Users WHERE username=?";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(query);
            stmt.setString(1, mUsername);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                User user = new User(rs.getString("username"), rs.getString("password"), rs.getString("name"), rs.getTimestamp("dateModified"));
                user.setId(rs.getString("id"));
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static boolean insertNewIPFSFile(IPFSFile file) {
        try {
            PreparedStatement statement = DatabaseHandler.getInstance().getConnection().prepareStatement(
                    "INSERT INTO FileInfo(id,filename,filehash,fileownerid,isPinned) VALUES(?,?,?,?,?)");
            statement.setString(1, file.getId());
            statement.setString(2, file.getFileName());
            statement.setString(3, file.getFileHash());
            statement.setString(4, AppPreference.getUser().getId());
            statement.setBoolean(5, file.getIsPinned());

            return statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean insertNewPeer(Peer peer) {
        try {
            PreparedStatement statement = DatabaseHandler.getInstance().getConnection().prepareStatement(
                    "INSERT INTO Peer(id,hash,name) VALUES(?,?,?)");
            statement.setString(1, peer.getId());
            statement.setString(2, peer.getHash());
            statement.setString(3, peer.getName());
            return statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean updatePeer(Peer peer) {
        try {
            String update = "UPDATE Peer SET NAME=?, HASH=?, FILECOUNT=? WHERE ID=?";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(update);
            stmt.setString(1, peer.getName());
            stmt.setString(2, peer.getHash());
            stmt.setInt(3, peer.getFilecount());
            stmt.setString(4, peer.getId());
            int res = stmt.executeUpdate();
            return (res > 0);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static List<MyFileList> FileLists(Boolean isMyFiles) {
        List<MyFileList> list = new ArrayList<>();
        String query = isMyFiles ? "SELECT F.ID AS ID,F.FILENAME AS NAME, F.FILEHASH AS HASH, U.NAME AS OWNER FROM FILEINFO AS F INNER JOIN USERS AS U ON U.ID = F.FILEOWNERID WHERE F.FILEOWNERID = '" + AppPreference.getUser().getId() + "'"
                : "SELECT F.ID AS ID, F.FILENAME AS NAME, F.FILEHASH AS HASH, U.NAME AS OWNER FROM FILEINFO AS F INNER JOIN USERS AS U ON U.ID = F.FILEOWNERID\n"
                + "UNION\n"
                + "SELECT UB.ID AS ID, UB.FILENAME AS NAME, UB.FILEHASH AS HASH, UB.OWNER AS OWNER FROM USERBLOCKS AS UB";
        DatabaseHandler handler = DatabaseHandler.getInstance();

        ResultSet rs = handler.execQuery(query);
        try {
            int row = 0;
            while (rs.next()) {
                String sn = String.valueOf(row = row + 1);
                String id = rs.getString("id");
                String name = rs.getString("name");
                String hash = rs.getString("hash");
                String owner = rs.getString("owner");

                list.add(new MyFileList(id, sn, name, hash, owner));

            }
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static List<IPFSFile> MyFileLists() {
        List<IPFSFile> list = new ArrayList<>();
        String query = "SELECT * FROM FILEINFO AS F WHERE F.FILEOWNERID = '" + AppPreference.getUser().getId() + "'";
        DatabaseHandler handler = DatabaseHandler.getInstance();

        ResultSet rs = handler.execQuery(query);
        try {
            while (rs.next()) {
                IPFSFile ipfsFile = new IPFSFile();
                ipfsFile.setId(rs.getString("id"));
                ipfsFile.setFileName(rs.getString("filename"));
                ipfsFile.setFileHash(rs.getString("filehash"));
                ipfsFile.setFileOwnerId(rs.getString("fileownerid"));
                ipfsFile.setIsPinned(false);
                ipfsFile.setDateAdded(rs.getTimestamp("dateadded"));
                ipfsFile.setDateModified(rs.getTimestamp("datemodified"));

                list.add(ipfsFile);

            }
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return list;
    }

    public static boolean updateFileHash(String id, String hash) {
        try {
            String update = "UPDATE FILEINFO SET FILEHASH=?,DATEMODIFIED=? WHERE ID=?";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(update);
            stmt.setString(1, hash);
            stmt.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            stmt.setString(3, id);
            int res = stmt.executeUpdate();
            return (res > 0);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean isFileExistById(String id) {
        try {
            String checkstmt = "SELECT COUNT(*) FROM fileinfo WHERE id=?";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(checkstmt);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int count = rs.getInt(1);
                System.out.println(count);
                return (count > 0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean insertSocketIPFSFile(IPFSFile file) {
        try {
            PreparedStatement statement = DatabaseHandler.getInstance().getConnection().prepareStatement(
                    "INSERT INTO FileInfo(id,filename,filehash,fileownerid,isPinned,dateadded,datemodified) VALUES(?,?,?,?,?,?,?)");
            statement.setString(1, file.getId());
            statement.setString(2, file.getFileName());
            statement.setString(3, file.getFileHash());
            statement.setString(4, file.getFileOwnerId());
            statement.setBoolean(5, file.getIsPinned());
            statement.setTimestamp(6, file.getDateAdded());
            statement.setTimestamp(7, file.getDateModified());

            return statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean insertInitData() {
        try {
            UUID uuid = UUID.randomUUID();
            String id = uuid.toString().replace("-", "").trim().toLowerCase().substring(0, 9);
            PreparedStatement statement = DatabaseHandler.getInstance().getConnection().prepareStatement(
                    "INSERT INTO GENBLOCK(id,peerid,filehash) VALUES(?,?,?)");
            statement.setString(1, id);
            statement.setString(2, "QmbxzQZQZX8XfvSUZVZN9o5X6PVpJY684LpsSKS6jdqSDM");
            statement.setString(3, "QmXQWwceJPuhPRkdCUMNbMXWF9MqJWDiFNdw4GR6zLnWTp");
            return statement.executeUpdate() > 0;
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static Optional<GenesisBlock> getGenBlockDetails() {
        try {
            String query = "SELECT * FROM GENBLOCK FETCH FIRST 1 ROWS ONLY";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(query);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                Boolean active = rs.getInt("active") == 1;
                GenesisBlock userBlock = new GenesisBlock(rs.getString("id"), rs.getString("peerid"), rs.getString("filehash"), active);
                return Optional.of(userBlock);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Optional.empty();
    }

    public static boolean isGenBlockActivated() {
        try {
            String checkstmt = "SELECT COUNT(*) FROM GENBLOCK WHERE active=?";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(checkstmt);
            stmt.setString(1, "1");
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                int count = rs.getInt(1);
                System.out.println(count);
                return (count > 0);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static Boolean doActivation() {
        try {
            String update = "UPDATE GENBLOCK SET ACTIVE=1 WHERE peerid=?";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(update);
            stmt.setString(1, "QmbxzQZQZX8XfvSUZVZN9o5X6PVpJY684LpsSKS6jdqSDM");
            int res = stmt.executeUpdate();
            return (res > 0);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    /**
     * Get a list of all users hash
     *
     * @return Optional list of string
     */
    public static Optional<List<String>> getUsersHash() {
        List<String> list = new ArrayList<>();
        String query = "SELECT FILENAME, FILEHASH FROM FILEINFO AS F WHERE F.FILEOWNERID = '" + AppPreference.getUser().getId() + "'";
        DatabaseHandler handler = DatabaseHandler.getInstance();

        ResultSet rs = handler.execQuery(query);
        try {
            while (rs.next()) {
                String arr = rs.getString("filename") + "->" + rs.getString("filehash");
                list.add(arr);
            }
            return Optional.of(list);
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    /**
     * Update Genesis Block
     *
     * @param genesisBlock The genesis block info
     * @return Boolean
     */
    public static Boolean updatGenesisBlock(GenesisBlock genesisBlock) {
        try {
            String update = "UPDATE GENBLOCK SET PEERID=?,FILEHASH=? WHERE ID=?";
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(update);
            stmt.setString(1, genesisBlock.getPeerId());
            stmt.setString(2, genesisBlock.getFileHash());
            stmt.setString(3, genesisBlock.getId());
            int res = stmt.executeUpdate();
            return (res > 0);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static boolean insertUserBlockBatch(List<String[]> files) {

        try {
            Connection dbconnection = DatabaseHandler.getInstance().getConnection();
            dbconnection.setAutoCommit(false);
            PreparedStatement statement = dbconnection.prepareStatement(
                    "INSERT INTO USERBLOCKS(id,filename,filehash,owner,dateAdded) VALUES(?,?,?,?,?)");
            files.stream().forEach(file -> {
                try {
                    UUID uuid = UUID.randomUUID();
                    String id = uuid.toString().replace("-", "").trim().toLowerCase().substring(0, 9);

                    statement.setString(1, id);
                    statement.setString(2, file[0]);
                    statement.setString(3, file[1]);
                    statement.setString(4, file[2]);
                    statement.setTimestamp(5, Timestamp.valueOf(LocalDateTime.now()));
                    statement.addBatch();
                } catch (SQLException ex) {
                    Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
                }
            });
            int[] arr = statement.executeBatch();
            dbconnection.commit();
            return arr.length > 0;
        } catch (SQLException ex) {
            Logger.getLogger(DataDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public static Boolean truncateTable(String tableName) {
        try {
            String update = "TRUNCATE TABLE " + tableName;
            PreparedStatement stmt = DatabaseHandler.getInstance().getConnection().prepareStatement(update);
            int res = stmt.executeUpdate();
            return (res > 0);
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
