/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.SocketSync;

import com.fasterxml.jackson.databind.ObjectMapper;
import ipfsapp.Preferences.AppPreference;
import ipfsapp.utils.IPFSUtil;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CraigDev
 */
public class SocketServer implements Runnable {

    private DatagramSocket socket;

    @Override
    public void run() {
        try {
            socket = new DatagramSocket(56545);
            socket.setBroadcast(true);
            ObjectMapper mapper = new ObjectMapper();

            while (true) {

                byte[] packetBuffer = new byte[65000];
                DatagramPacket packet = new DatagramPacket(packetBuffer, packetBuffer.length);
                socket.receive(packet);

                ClientPacket cp = mapper.readValue(new String(packet.getData()).trim(), ClientPacket.class);

                if (!cp.getSOCKET_ID().equals(AppPreference.getSOCKET_ID())) {
                    System.out.println("\n\n\n" + getClass().getName() + ">>> Socket Connection Initiated From Exernal host!");

                    ServerHelper socSerer = new ServerHelper(socket, packet, cp);
                    IPFSUtil.SOCKET_THREAD_POOL.submit(socSerer);
                }

            }

        } catch (SocketException | UnknownHostException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SocketServer.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
