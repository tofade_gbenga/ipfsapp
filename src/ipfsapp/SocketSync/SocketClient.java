/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.SocketSync;

import com.fasterxml.jackson.databind.ObjectMapper;
import ipfsapp.Preferences.AppPreference;
import ipfsapp.utils.IPFSUtil;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InterfaceAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CraigDev
 */
public class SocketClient implements Runnable {
    
    DatagramSocket clientSocket;
    
    @Override
    public void run() {
        try {
            Thread.sleep(6000);
            
            clientSocket = new DatagramSocket();
            clientSocket.setBroadcast(true);
            
            ClientPacket cp = new ClientPacket();
            cp.setSOCKET_ID(AppPreference.getSOCKET_ID());
            cp.setSocketData("fileinfo,peer,user");
            
            ObjectMapper mapper = new ObjectMapper();

            String toSend = mapper.writeValueAsString(cp);
            byte[] packetData = toSend.getBytes();

//            DatagramPacket packet = new DatagramPacket(packetData, packetData.length, InetAddress.getByName("25.255.255.255"), 56545);
//            clientSocket.send(packet);
//            System.out.println(getClass().getName() + ">>> Request packet sent to: 255.255.255.255 (DEFAULT)");
            // Broadcast the message over all the network interfaces
            Enumeration interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface network = (NetworkInterface) interfaces.nextElement();
                
                if (network.isLoopback() || !network.isUp()) {
                    continue;
                }
                
                for (InterfaceAddress interfaceAddresse : network.getInterfaceAddresses()) {
                    InetAddress broadcast = interfaceAddresse.getBroadcast();
                    if (broadcast == null) {
                        continue;
                    }
                    DatagramPacket sendPacket = new DatagramPacket(packetData, packetData.length, broadcast, 56545);
                    clientSocket.send(sendPacket);
                    
                    System.out.println(getClass().getName() + ">>> Request packet sent to: " + broadcast.getHostAddress() + "; Interface: " + network.getDisplayName());
                }
                ClientHelper helper = new ClientHelper(clientSocket);
                IPFSUtil.SOCKET_THREAD_POOL.submit(helper);
            }
            
        } catch (SocketException | UnknownHostException ex) {
            Logger.getLogger(SocketClient.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException | InterruptedException ex) {
            Logger.getLogger(SocketClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
