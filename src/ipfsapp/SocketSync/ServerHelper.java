/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.SocketSync;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.node.ObjectNode;
import ipfsapp.DataHandler.DataDAO;
import ipfsapp.Model.IPFSFile;
import ipfsapp.Preferences.AppPreference;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CraigDev
 */
public class ServerHelper implements Runnable {

    DatagramSocket socket;
    DatagramPacket packet;
    ClientPacket cp;

    public ServerHelper(DatagramSocket socket, DatagramPacket packet, ClientPacket cp) {
        this.socket = socket;
        this.packet = packet;
        this.cp = cp;
    }

    @Override
    public void run() {
        try {
            //Packet received
            System.out.println(getClass().getName() + ">>> Client Socket Data packet received from: " + packet.getAddress().getHostAddress());
            System.out.println(getClass().getName() + ">>> Packet received; data: " + new String(packet.getData()).trim());

            String toSend = "{}";
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            //Do data handling here
            JsonNode rootNode = mapper.createObjectNode();
            String[] commands = cp.getSocketData().split(",");

            if (commands.length > 0) {
                for (String command : commands) {
                    switch (command) {
                        case "fileinfo":
                            List<IPFSFile> list = DataDAO.MyFileLists();
                            String files = mapper.writeValueAsString(list);
                            ((ObjectNode) rootNode).set(command, mapper.readTree(files));
                            break;
                        case "peer":
                            break;
                        case "user":
                            String user = mapper.writeValueAsString(AppPreference.getUser());
                            ((ObjectNode) rootNode).set(command, mapper.readTree(user));
                            break;
                    }
                }

                toSend = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(rootNode);
            }

            packet.setData(toSend.getBytes());
            packet.setLength(toSend.length());
            socket.send(packet);

            System.out.println(getClass().getName() + ">>> Sent packet to: " + packet.getAddress().getHostAddress());

        } catch (IOException ex) {
            Logger.getLogger(ServerHelper.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
