/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.SocketSync;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import ipfsapp.DataHandler.DataDAO;
import ipfsapp.Model.IPFSFile;
import ipfsapp.Model.User;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author CraigDev
 */
public class ClientHelper implements Runnable {

    DatagramSocket clientSocket;

    public ClientHelper(DatagramSocket clientSocket) {
        this.clientSocket = clientSocket;
    }

    @Override
    public void run() {
        ObjectMapper mapper = new ObjectMapper();
        while (true) {

            //Wait for a response
            byte[] recvBuf = new byte[65000];
            DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
            try {
                clientSocket.receive(receivePacket);

                String jsonResponse = new String(receivePacket.getData()).trim();

                JsonNode rootNode = mapper.readValue(jsonResponse, JsonNode.class);

                User user = mapper.readValue(rootNode.get("user").toString(), User.class);
                if (!DataDAO.isUserExistById(user.getId())) {
                    DataDAO.insertSocketNewUser(user);
                }

                List<IPFSFile> files = mapper.readValue(rootNode.get("fileinfo").toString(), new TypeReference<List<IPFSFile>>() {
                });

                files.stream().forEach((file) -> {
                    if (!DataDAO.isFileExistById(file.getId())) {
                        DataDAO.insertSocketIPFSFile(file);
                    }
                });
            } catch (IOException ex) {
                Logger.getLogger(ClientHelper.class.getName()).log(Level.SEVERE, null, ex);
            }

//            System.out.println(getClass().getName() + ">>> Broadcast response from server: " + receivePacket.getAddress().getHostAddress());
//            System.out.println(getClass().getName() + ">>> Broadcast response Data: " + new String(receivePacket.getData()).trim());
        }
    }

}
