/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.SocketSync;

/**
 *
 * @author CraigDev
 */
public class ClientPacket {

    private String SOCKET_ID;
    private String socketData;

    public void setSOCKET_ID(String SOCKET_ID) {
        this.SOCKET_ID = SOCKET_ID;
    }

    public String getSOCKET_ID() {
        return this.SOCKET_ID;
    }

    public String getSocketData() {
        return socketData;
    }

    public void setSocketData(String socketData) {
        this.socketData = socketData;
    }

}
