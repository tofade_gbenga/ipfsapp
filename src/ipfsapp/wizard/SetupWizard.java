/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.wizard;

import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ButtonType;
import org.controlsfx.dialog.Wizard;
import org.controlsfx.dialog.WizardPane;

/**
 *
 * @author CraigDev
 */
public class SetupWizard extends Wizard {

    private static Boolean isSetupDone = false;

    private static Boolean isInitDone = false;

    private static Boolean isFinalDone = false;

    public static Boolean IsSetupDone() {
        return isSetupDone;
    }

    public static void setIsSetupDone(Boolean aIsSetupDone) {
        isSetupDone = aIsSetupDone;
    }

    public static Boolean IsInitDone() {
        return isInitDone;
    }

    public static void setIsInitDone(Boolean isInitDone) {
        SetupWizard.isInitDone = isInitDone;
    }

    public static Boolean IsFinalDone() {
        return isFinalDone;
    }

    public static void setIsFinalDone(Boolean isFinalDone) {
        SetupWizard.isFinalDone = isFinalDone;
    }

    public void doInit() {
        WizardPane setupPage = new WizardPane();
        SetupPageController setupPageController = (SetupPageController) URLToFXMLLoader(setupPage, "SetupPage.fxml");
        setupPageController.accessWizardPane(setupPage.getButtonTypes());

        WizardPane peerPage = new WizardPane();
        PeerPageController peerPageController = (PeerPageController) URLToFXMLLoader(peerPage, "PeerPage.fxml");
        peerPageController.accessWizardPane(setupPage.getButtonTypes());

        WizardPane finalPage = new WizardPane();
        FinalPageController finalPageController = (FinalPageController) URLToFXMLLoader(finalPage, "FinalPage.fxml");
        finalPageController.accessWizardPane(setupPage.getButtonTypes());

        setFlow(new LinearFlow(setupPage, peerPage, finalPage));

        showAndWait().ifPresent(result -> {
            if (result == ButtonType.FINISH) {
                if (isFinalDone) {
                    IPFSUtil.loadWindow(getClass().getResource("/ipfsapp/ui/MainView/MainView.fxml"), "Dashboard", null);
                    AlertMaker.showSimpleAlert("Completed", "Finished Setting Up IPFS");
                } else {
                    AlertMaker.showSimpleAlert("Incomplete", "Setup could not be finalized please go back and check.");
                }
            }
        });
    }

    private Object URLToFXMLLoader(WizardPane page, String stringURL) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource(stringURL));
            page.setContent(loader.load());
            return loader.getController();
        } catch (IOException ex) {
            Logger.getLogger(SetupWizard.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
}
