/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.wizard;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXProgressBar;
import ipfsapp.DaemonManager.DaemonManager;
import ipfsapp.DaemonManager.IPFSHandler;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import javafx.collections.ObservableList;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;

/**
 * FXML Controller class
 *
 * @author CraigDev
 */
public class SetupPageController implements Initializable {

    @FXML
    private JFXButton setupButton;
    @FXML
    private Label messageLabel;
    @FXML
    private JFXProgressBar loader;

    private Boolean isSetup = false;

    private ScheduledFuture future;
    @FXML
    private StackPane rootPane;
    @FXML
    private VBox rootBox;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
    }

    @FXML
    private void handleStartSetup(ActionEvent event) {
        if (IPFSUtil.connectivity()) {

            if (!DaemonManager.isDaemonRunning()) {
                DaemonManager manager = new DaemonManager();
            }
            messageLabel.setVisible(true);
            loader.setVisible(true);
            setupButton.setDisable(true);
            checkIfInstallationCompleted();
        } else {
            AlertMaker.showMaterialDialog(rootPane, rootBox, new ArrayList<>(), "No Internet Connection", IPFSUtil.NO_INTERNET);
        }
    }

    public void accessWizardPane(ObservableList<ButtonType> buttons) {
    }

    private void checkIfInstallationCompleted() {

        ScheduledService<Boolean> svc = new ScheduledService<Boolean>() {
            @Override
            protected Task<Boolean> createTask() {
                return new Task<Boolean>() {
                    @Override
                    protected Boolean call() throws Exception {
                        String ipfsConfig = IPFSUtil.getAppDirectory() + File.separator + ".ipfs" + File.separator + "config";
                        File ipfsConfigFile = new File(ipfsConfig);
                        return ipfsConfigFile.exists();
                    }
                };
            }
        };

        svc.setDelay(Duration.seconds(30));
        svc.setPeriod(Duration.seconds(30));

        svc.setOnSucceeded(e -> {
            isSetup = svc.getValue();
            System.out.println(isSetup ? "task done" : "task not done");
            SetupWizard.setIsInitDone(isSetup);
            if (isSetup) {
                if (DaemonManager.isDaemonRunning()) {
                    DaemonManager.stopDaemon();
                }
                IPFSHandler.setupInitPeerKey();
                DaemonManager daemonManager = new DaemonManager();
                loader.setVisible(false);
                svc.cancel();
                messageLabel.setText("IPFS Installation Completed, Please Click Next To Continue Setting Up Your Name Space.");
            }
        });

        svc.start();
    }

}
