/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.wizard;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXProgressBar;
import ipfsapp.DataHandler.DataDAO;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author CraigDev
 */
public class FinalPageController implements Initializable {

    @FXML
    private JFXButton setupButton;
    @FXML
    private Label messageLabel;
    @FXML
    private StackPane rootPane;
    @FXML
    private VBox rootBox;
    @FXML
    private JFXProgressBar loader;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

    public void accessWizardPane(ObservableList<ButtonType> buttonTypes) {
    }

    @FXML
    private void handleFinishSetup(ActionEvent event) {
        if (SetupWizard.IsSetupDone()) {
            loader.setVisible(true);
            messageLabel.setVisible(true);
            setupButton.setDisable(true);
            doActivation();
        } else {
            AlertMaker.showMaterialDialog(rootPane, rootBox, new ArrayList<>(), "Incomplete Setup", "Setup not completed plase go back and complete setup");
        }
    }

    private void doActivation() {
        Task<Boolean> task = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                updateMessage("Finalizing setup... please hold on...");
                Boolean isActivated = DataDAO.doActivation();
                updateMessage("Setup Completed, Click Finish To Start Your Application.");
                return isActivated;
            }
        };

        if (messageLabel.textProperty().isBound()) {
            messageLabel.textProperty().unbind();
        }
        messageLabel.textProperty().bind(task.messageProperty());

        task.setOnSucceeded(e -> {
            loader.setVisible(false);
            messageLabel.textProperty().unbind();
            SetupWizard.setIsFinalDone(true);
        });
        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

}
