/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.wizard;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXProgressBar;
import io.ipfs.multihash.Multihash;
import ipfsapp.DaemonManager.IPFSHandler;
import ipfsapp.DataHandler.DataDAO;
import ipfsapp.Model.GenesisBlock;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 * FXML Controller class
 *
 * @author CraigDev
 */
public class PeerPageController implements Initializable {
    
    @FXML
    private JFXButton setupButton;
    @FXML
    private Label messageLabel;
    @FXML
    private JFXProgressBar loader;
    @FXML
    private StackPane rootPane;
    @FXML
    private VBox rootBox;
    
    private IPFSHandler iPFSHandler;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        iPFSHandler = new IPFSHandler();
    }
    
    public void accessWizardPane(ObservableList<ButtonType> buttonTypes) {
    }
    
    @FXML
    private void handlePeerSetup(ActionEvent event) {
        if (SetupWizard.IsInitDone()) {
            if (IPFSUtil.connectivity()) {
                loader.setVisible(true);
                messageLabel.setVisible(true);
                setupButton.setDisable(true);
                addAndPublishPeer();
            } else {
                AlertMaker.showMaterialDialog(rootPane, rootBox, new ArrayList<>(), "No Internet Connection", IPFSUtil.NO_INTERNET);
            }
        } else {
            AlertMaker.showMaterialDialog(rootPane, rootBox, new ArrayList<>(), "Incomplete Init", "Initialization not completed plase go back and complete Initialization");
            
        }
        
    }
    
    private void ResolveAndDownloadPeerName() {
        Task<Boolean> task = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                updateMessage("Downloading and Initializing IPFS Name Space... Please kindly hold on Till this is completed.");
                Boolean fileStatus = IPFSHandler.setupInitPeerKey();
                return fileStatus;
            }
        };
        if (messageLabel.textProperty().isBound()) {
            messageLabel.textProperty().unbind();
        }
        messageLabel.textProperty().bind(task.messageProperty());
        
        task.setOnSucceeded(e -> {
            messageLabel.textProperty().unbind();
            addAndPublishPeer();
        });
        
        IPFSUtil.TASK_EXECUTOR.execute(task);
    }
    
    private void addAndPublishPeer() {
        Task<Boolean> task = new Task<Boolean>() {
            @Override
            protected Boolean call() throws Exception {
                GenesisBlock block = null;
                
                updateMessage("Done Initiating And Downloading Name Space, Hold on while you node is added to the Namespace and republished.");
                
                Optional<String> ipfsHash = iPFSHandler.resolveGenesisNameSpace();
                
                if (!ipfsHash.isPresent()) {
                    Optional<GenesisBlock> genBlock = DataDAO.getGenBlockDetails();
                    block = genBlock.get();
                    iPFSHandler.publishPeerID("PeerID-83fd7f041", block.getFileHash());
                    ipfsHash = Optional.of(block.getFileHash());
                    
                }
                
                updateMessage("Done Resolving Name Space, adding your node to the Namespace");
                Optional<Multihash> newHash = iPFSHandler.addPeerToNameSpace(ipfsHash);
                if (newHash.isPresent()) {
                    updateMessage("Done Adding your peer to the Name Space, please wait while the Name Space is republished"
                            + " please make sure you have internet connection, this may take some time.");
                    iPFSHandler.publishPeerID("PeerID-83fd7f041", newHash.get());
                    block.setFileHash(newHash.get().toBase58());
                    updateMessage("Done republishing Name Space, click on next to finalize setup");
                    
                    Boolean isUpdated = DataDAO.updatGenesisBlock(block);
                    
                    return true;
                } else {
                    updateMessage("An Error Occured , Your Node could not be added to the Name Space");
                    return false;
                }
                
            }
        };
        if (messageLabel.textProperty().isBound()) {
            messageLabel.textProperty().unbind();
        }
        messageLabel.textProperty().bind(task.messageProperty());
        
        task.setOnSucceeded(e -> {
            loader.setVisible(false);
            messageLabel.textProperty().unbind();
            SetupWizard.setIsSetupDone(task.getValue());
        });
        
        IPFSUtil.TASK_EXECUTOR.execute(task);
    }
    
}
