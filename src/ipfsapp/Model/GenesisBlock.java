/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.Model;

/**
 *
 * @author CraigDev
 */
public class GenesisBlock {

    private String id;
    private String peerId;
    private String fileHash;
    private boolean active;

    public GenesisBlock(String id, String peerId, String fileHash,Boolean active) {
        this.id = id;
        this.peerId = peerId;
        this.fileHash = fileHash;
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPeerId() {
        return peerId;
    }

    public void setPeerId(String peerId) {
        this.peerId = peerId;
    }

    public String getFileHash() {
        return fileHash;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
    
    

}
