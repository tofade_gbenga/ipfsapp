/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.Model;

import java.sql.Timestamp;
import java.util.UUID;

/**
 *
 * @author CraigDev
 */
public class User {

    private String id;
    private String username;
    private String password;
    private String name;
    private Timestamp dateModified;

    public User() {
    }

    public User(String username, String password, String name, Timestamp dateModified) {
        UUID uuid = UUID.randomUUID();
        this.id = uuid.toString().replace("-", "").trim().toLowerCase().substring(0, 9);
        this.username = username;
        this.password = password;
        this.name = name;
        this.dateModified = dateModified;
    }

    public User(String username, String password, String name) {
        UUID uuid = UUID.randomUUID();
        this.id = uuid.toString().replace("-", "").trim().toLowerCase().substring(0, 9);
        this.username = username;
        this.password = password;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

}
