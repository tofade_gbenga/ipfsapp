/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.Model;

import java.sql.Timestamp;
import java.util.UUID;

/**
 *
 * @author CraigDev
 */
public class IPFSFile {

    private String id;
    private String fileName;
    private String fileHash;
    private String fileOwnerId;
    private Boolean isPinned;
    private Timestamp dateAdded;
    private Timestamp dateModified;

    public IPFSFile() {
    }

    public IPFSFile(String fileName, String fileHash, String fileOwnerId) {
        UUID uuid = UUID.randomUUID();
        this.id = uuid.toString().replace("-", "").trim().toLowerCase().substring(0, 9);
        this.fileName = fileName;
        this.fileHash = fileHash;
        this.fileOwnerId = fileOwnerId;
    }

    public IPFSFile(String fileName, String fileHash, String fileOwnerId, Boolean isPinned) {
        UUID uuid = UUID.randomUUID();
        this.id = uuid.toString().replace("-", "").trim().toLowerCase().substring(0, 9);
        this.fileName = fileName;
        this.fileHash = fileHash;
        this.fileOwnerId = fileOwnerId;
        this.isPinned = isPinned;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileHash() {
        return fileHash;
    }

    public void setFileHash(String fileHash) {
        this.fileHash = fileHash;
    }

    public String getFileOwnerId() {
        return fileOwnerId;
    }

    public void setFileOwnerId(String fileOwnerId) {
        this.fileOwnerId = fileOwnerId;
    }

    public Boolean getIsPinned() {
        return isPinned;
    }

    public void setIsPinned(Boolean isPinned) {
        this.isPinned = isPinned;
    }

    public Timestamp getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(Timestamp dateAdded) {
        this.dateAdded = dateAdded;
    }

    public Timestamp getDateModified() {
        return dateModified;
    }

    public void setDateModified(Timestamp dateModified) {
        this.dateModified = dateModified;
    }

}
