/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp;

import ipfsapp.DaemonManager.DaemonManager;
import ipfsapp.DataHandler.DatabaseHandler;
import ipfsapp.utils.IPFSUtil;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author CraigDev
 */
public class IPFSApp extends Application {

    private Stage defaultStage;

    @Override
    public void start(Stage stage) throws Exception {
        // initiate DB
        initDB();
        this.defaultStage = stage;

    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() {
        if (DaemonManager.isDaemonRunning()) {
            DaemonManager.ipfsDaemon.stop();
        }
        System.exit(0);
    }

    /**
     * Initialize db in background thread
     */
    private void initDB() {
        Task<Integer> task = new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                DatabaseHandler.getInstance();
                return null;
            }
        };

        task.setOnSucceeded(e -> loadStage());
        IPFSUtil.TASK_EXECUTOR.execute(task);
    }

    

    /**
     * Load stage
     */
    private void loadStage() {
        try {

            //Set Scene
            Parent root = FXMLLoader.load(getClass().getResource("ui/SignIn/login.fxml"));

            Scene scene = new Scene(root);

            defaultStage.setScene(scene);
            defaultStage.show();
        } catch (IOException ex) {
            Logger.getLogger(IPFSApp.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    

}
