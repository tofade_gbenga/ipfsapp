/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.DaemonManager;

import io.ipfs.api.IPFS;
import io.ipfs.api.KeyInfo;
import io.ipfs.api.MerkleNode;
import io.ipfs.api.NamedStreamable;
import io.ipfs.multiaddr.MultiAddress;
import io.ipfs.multihash.Multihash;
import ipfsapp.DataHandler.DataDAO;
import ipfsapp.Model.GenesisBlock;
import ipfsapp.Model.IPFSFile;
import ipfsapp.Model.Peer;
import ipfsapp.Preferences.AppPreference;
import ipfsapp.utils.AlertMaker;
import ipfsapp.utils.IPFSUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;
import javafx.concurrent.Task;
import javafx.scene.control.Alert;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author CraigDev
 */
public class IPFSHandler {

    public static IPFS ipfs;

    public IPFSHandler() {
        ipfs = DaemonManager.getIPFS();
    }

    public static IPFS getIpfs() {
        return ipfs;
    }

    public static void setIpfs(IPFS ipfs) {
        IPFSHandler.ipfs = ipfs;
    }

    public Optional<IPFSFile> addFile(File source) {
        IPFSFile ipfsFile = null;
        try {

            //Add file to IPFS
            NamedStreamable file = new NamedStreamable.FileWrapper(source);
            MerkleNode node = ipfs.add(file).get(0);
            Multihash pointer = node.hash;
            ipfsFile = new IPFSFile(source.getName(), pointer.toString(), AppPreference.getUser().getId(), Boolean.TRUE);
            Boolean isInserted = DataDAO.insertNewIPFSFile(ipfsFile);

            if (isInserted) {
                return Optional.of(ipfsFile);
            } else {
                return Optional.empty();
            }
            //return the file
        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    public Optional<IPFSFile> addNote(String noteTitle, String noteContent) {
        IPFSFile ipfsFile = null;
        try {
            //Add note to IPFS
            NamedStreamable file = new NamedStreamable.ByteArrayWrapper(noteTitle, noteContent.getBytes());
            MerkleNode node = ipfs.add(file).get(0);
            Multihash pointer = node.hash;
            ipfsFile = new IPFSFile(noteTitle, pointer.toString(), AppPreference.getUser().getId(), Boolean.TRUE);
            Boolean isInserted = DataDAO.insertNewIPFSFile(ipfsFile);

            if (isInserted) {
                return Optional.of(ipfsFile);
            } else {
                return Optional.empty();
            }
        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    public Boolean saveEditedContent(String id, String title, String content) throws IOException {
        NamedStreamable file = new NamedStreamable.ByteArrayWrapper(title, content.getBytes());
        MerkleNode node = ipfs.add(file).get(0);
        Multihash pointer = node.hash;
        return DataDAO.updateFileHash(id, pointer.toString());
    }

    public Optional<KeyInfo> generatePeerID(String name) throws IOException {
        UUID uuid = UUID.randomUUID();
        String newname = name + "-" + uuid.toString().replace("-", "").trim().toLowerCase().substring(0, 9);
        Optional<KeyInfo> peerID = getAllPeerIDs().filter(key -> key.name.equalsIgnoreCase(newname)).findFirst();
        return peerID.isPresent() ? Optional.empty() : Optional.of(ipfs.key.gen(newname, Optional.of("rsa"), Optional.of("2048")));
    }

    public Stream<KeyInfo> getAllPeerIDs() {
        try {
            return ipfs.key.list().stream();
        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public void ipfsGC() throws IOException {
        ipfs.repo.gc();
    }

    public Object publishPeerID(String peerID, Multihash hash) {
        try {
            return peerID != null ? ipfs.name.publish(hash, Optional.of(peerID)) : ipfs.name.publish(hash);
        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    public Object publishPeerID(String peerID, String hash) {
        try {
            Multihash multihash = Multihash.fromBase58(hash);
            return peerID != null ? ipfs.name.publish(multihash, Optional.of(peerID)) : ipfs.name.publish(multihash);
        } catch (Exception ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

    /*
    republish ipns
     */
    private void rePublishIPNS() {
        Task task = new Task() {
            @Override
            protected Object call() throws Exception {
                return null;
            }
        };

    }

    public Optional<String> getHashConent(String hash) {
        try {
            Multihash pointer = Multihash.fromBase58(hash);
            byte[] content = ipfs.cat(pointer);
            return Optional.of(new String(content));
        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    public Optional<List<io.ipfs.api.Peer>> getConnectedPeers() {
        try {
            Optional<List<io.ipfs.api.Peer>> peers = Optional.of(ipfs.swarm.peers());
            return peers;

        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    public String getMyPeerId() {
        try {
            Stream<KeyInfo> info = ipfs.key.list().stream();
            Optional<KeyInfo> self = info.filter(peer -> peer.name.equalsIgnoreCase("self")).findAny();
            if (self.isPresent()) {
                return self.get().id.toBase58();
            }
        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static Boolean setupInitPeerKey() {
        try {
            String destination = IPFSUtil.getAppDirectory() + File.separator + ".ipfs";

            String jarFile;

            if (new File(IPFSUtil.getAppDirectory() + File.separator + "dist").exists()) {
                jarFile = IPFSUtil.getAppDirectory() + File.separator + "dist" + File.separator + "lib" + File.separator + "peer.jar";
            } else {
                jarFile = IPFSUtil.getAppDirectory() + File.separator + "lib" + File.separator + "peer.jar";
            }

            String tempDirectory = IPFSUtil.getAppDirectory() + File.separator + "temp";

            IPFSUtil.unzip(jarFile, new File(tempDirectory));

            //Delete Directory
            FileUtils.deleteDirectory(new File(destination + File.separator + "blocks"));
            FileUtils.deleteDirectory(new File(destination + File.separator + "datastore"));
            FileUtils.deleteDirectory(new File(destination + File.separator + "keystore"));

            // Copy New Directory
            FileUtils.copyDirectoryToDirectory(new File(tempDirectory + File.separator + "blocks"), new File(destination));
            FileUtils.copyDirectoryToDirectory(new File(tempDirectory + File.separator + "datastore"), new File(destination));
            FileUtils.copyDirectoryToDirectory(new File(tempDirectory + File.separator + "keystore"), new File(destination));
            FileUtils.copyDirectoryToDirectory(new File(tempDirectory + File.separator + "META-INF"), new File(destination));

            FileUtils.deleteDirectory(new File(tempDirectory));
            return true;
//            return createFileFromIPFS(fileDest, fileHash);
        } catch (Exception ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Resolve ipns to ipfs string
     *
     * @param ipns
     * @return Optional of String
     */
    public Optional<String> resolveIPNS(String ipns) {
        try {
            Multihash hash = Multihash.fromBase58(ipns);
            return Optional.of(ipfs.name.resolve(hash));
        } catch (Exception ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
            return Optional.empty();
        }
    }

    public boolean unpinBlock(String fileHash) {
        try {
            Multihash hash = Multihash.fromBase58(fileHash);
            List<Multihash> hashes = ipfs.pin.rm(hash);
            return !hashes.isEmpty();
        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;

    }

    public boolean pinBlock(String fileHash) {
        try {
            Multihash hash = Multihash.fromBase58(fileHash);
            List<Multihash> hashes = ipfs.pin.add(hash);
            return !hashes.isEmpty();
        } catch (Exception ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public boolean pinBlock(Multihash fileHash) {
        try {

            List<Multihash> hashes = ipfs.pin.add(fileHash);
            return !hashes.isEmpty();
        } catch (Exception ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }

    public void config() {
        try {
            Map map = ipfs.config.show();
            System.out.println(map.toString());
        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Optional<String> resolveGenesisNameSpace() {
        GenesisBlock genBlock = null;
        Optional<GenesisBlock> genesisBlockOptional = DataDAO.getGenBlockDetails();

        if (genesisBlockOptional.isPresent()) {
            genBlock = genesisBlockOptional.get();
            // resolve ipns of genesis block
            return resolveIPNS(genBlock.getPeerId());
        }
        return Optional.empty();
    }

    public Optional<Multihash> addPeerToNameSpace(Optional<String> optString) {

        if (optString.isPresent()) {
            String ipfsString = optString.get().replace("/ipfs/", "");
            Boolean pinned = pinBlock(ipfsString);
            Optional<String> content = getHashConent(ipfsString);
            if (content.isPresent()) {
                //convert to list of users
                List<String> listOfUserPeer = new ArrayList<>(IPFSUtil.convertStringToList(content.get()));

                //check if user exists
                Optional<String> me = listOfUserPeer.stream().filter(user -> user.endsWith(getMyPeerId())).findFirst();
                if (!me.isPresent()) {
                    try {
                        String userDetails = AppPreference.getUser().getName() + "->" + getMyPeerId();
                        listOfUserPeer.add(userDetails);

                        String newContent = IPFSUtil.createStringFromList(listOfUserPeer);

                        //Add note to IPFS
                        NamedStreamable file = new NamedStreamable.ByteArrayWrapper("usergenblock.txt", newContent.getBytes());
                        MerkleNode node = ipfs.add(file).get(0);
                        pinBlock(node.hash);
                        return Optional.of(node.hash);
                    } catch (Exception ex) {
                        Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
                        return Optional.empty();
                    }
                } else {
                    Multihash hash = Multihash.fromBase58(ipfsString);
                    return Optional.of(hash);
                }
            }

        } else {
            return Optional.empty();
        }
        return Optional.empty();
    }

    /**
     *
     */
    public void addUserToGenBlock() {
        GenesisBlock genBlock = null;
        Multihash newpointer = null;
        List<String> hashesToUnpin = new ArrayList<>();
        //get genesis bock details
        Optional<GenesisBlock> genesisBlockOptional = DataDAO.getGenBlockDetails();
        if (genesisBlockOptional.isPresent()) {
            genBlock = genesisBlockOptional.get();
            // resolve ipns of genesis block
            Optional<String> ipnsOptional = resolveIPNS(genBlock.getPeerId());

            if (ipnsOptional.isPresent()) {
                String ipfsString = ipnsOptional.get().replace("/ipfs/", "");
                // pin resolved ipfs file
                Boolean pinned = pinBlock(ipfsString);
                if (pinned) {
                    hashesToUnpin.add(ipfsString);
                    // get content of ipfs file
                    Optional<String> content = getHashConent(ipfsString);
                    if (content.isPresent()) {
                        //convert to list of users
                        List<String> listOfUserPeer = new ArrayList<>(IPFSUtil.convertStringToList(content.get()));

                        //check if user exists
                        Optional<String> me = listOfUserPeer.stream().filter(user -> user.startsWith(AppPreference.getUser().getName())).findFirst();
                        if (me.isPresent()) {
                            try {
                                String userDetails = AppPreference.getUser().getName() + "->" + getMyPeerId();
                                listOfUserPeer.add(userDetails);

                                String newContent = IPFSUtil.createStringFromList(listOfUserPeer);

                                //Add note to IPFS
                                NamedStreamable file = new NamedStreamable.ByteArrayWrapper("usergenblock.txt", newContent.getBytes());
                                MerkleNode node = ipfs.add(file).get(0);
                                newpointer = node.hash;
                                hashesToUnpin.add(newpointer.toBase58());
                            } catch (IOException ex) {
                                Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        }
                    }

                    if (newpointer != null) {
                        try {
                            // publish new content to ipns
                            Map map = ipfs.name.publish(newpointer, Optional.of("PeerID-83fd7f041"));
//                            hashesToUnpin.stream().forEach(pin -> unpinBlock(pin));
//                            ipfsGC();
                        } catch (IOException ex) {
                            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }

                } else {
                    AlertMaker.showErrorMessage("Error", "Couldn't pin File an error occured!!!");
                }
            } else {
                AlertMaker.showErrorMessage("Error", "Couldn't Resolve IPNS, an error occured!!!");
                return;
            }

        } else {
            return;
        }
    }

    /**
     *
     * @param fileDest destination directory
     * @param filehash hash to read from IPFS
     * @return Boolean if the file was written
     */
    public Boolean createFileFromIPFS(String fileDest, String filehash) {
        try (FileOutputStream fileOuputStream = new FileOutputStream(fileDest)) {
            Multihash hash = Multihash.fromBase58(filehash);
            byte[] contentByte = ipfs.cat(hash);
            fileOuputStream.write(contentByte);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    /**
     * Rebroadcast Hashes
     *
     * @return if the file was broadcasted
     */
    public boolean rebroadcastHashes() {
        Optional<List<String>> hashes = DataDAO.getUsersHash();
        if (hashes.isPresent()) {
            try {
                List<String> arrList = new ArrayList<>(hashes.get());

                String content = IPFSUtil.createStringFromList(arrList);

                //Add note to IPFS
                NamedStreamable file = new NamedStreamable.ByteArrayWrapper("fileblock", content.getBytes());
                MerkleNode node = ipfs.add(file).get(0);
                Multihash pointer = node.hash;

                return publishPeerID(null, pointer) != null;

            } catch (IOException ex) {
                Logger.getLogger(IPFSHandler.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }

        }
        return false;
    }

    /**
     * Rebroadcast Name Space and republish if namespace has expired
     *
     * @return Boolean
     */
    public Boolean reBroadCastNameSpace() {
        // Resolve genesis name space
        Optional<String> ipfsOptional = resolveGenesisNameSpace();
        GenesisBlock genesisBlock = DataDAO.getGenBlockDetails().get();

        if (ipfsOptional.isPresent()) {
            // get genesis block details
            String ipfsHash = ipfsOptional.get().replace("/ipfs/", "");
            // if the ipfs string is different, then update the genesis block.
            if (!ipfsHash.equalsIgnoreCase(genesisBlock.getFileHash())) {
                genesisBlock.setFileHash(ipfsHash);
                Boolean isUpdated = DataDAO.updatGenesisBlock(genesisBlock);
            }

        }

        //republish peer
        Object publishedObject = publishPeerID(genesisBlock.getPeerId(), genesisBlock.getFileHash());
        return publishedObject != null;
    }

    
    /**
     * Gets a list of all users files in the name space
     *
     * @return
     */
    public List<String[]> resolveAllPeerFiles() {
        List<String[]> listOfFiles = new ArrayList<>();

        Optional<String> ipfsOptional = resolveGenesisNameSpace();

        if (ipfsOptional.isPresent()) {
            String ipfsHash = ipfsOptional.get().replace("/ipfs/", "");
            Optional<String> peerAndOwner = getHashConent(ipfsHash);

            if (peerAndOwner.isPresent()) {

                List<String> listOfeers = new ArrayList<>(IPFSUtil.convertStringToList(peerAndOwner.get()));
                listOfeers.stream().forEach(peer -> {
                    String[] str = peer.split("->");
                    String peerOwner = str[0];
                    String peerid = str[1];

                    if (!peerid.equalsIgnoreCase(getMyPeerId())) {
                        Optional<String> contentHash = resolveIPNS(peerid);
                        if (contentHash.isPresent()) {
                            String ipfsContentHash = contentHash.get().replace("/ipfs/", "");
                            Optional<String> hashes = getHashConent(ipfsContentHash);
                            if (hashes.isPresent()) {
                                List<String> usersHash = new ArrayList<>(IPFSUtil.convertStringToList(hashes.get()));

                                usersHash.stream().forEach(hash -> {
                                    String[] arr = hash.split("->");
                                    String[] ownerFile = {arr[0], arr[1], peerOwner};
                                    listOfFiles.add(ownerFile);
                                });
                            }
                        }
                    }

                });
                return listOfFiles;
            }

        }
        return new ArrayList<>();
    }

}
