/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ipfsapp.DaemonManager;

import fr.rhaz.events.EventRunnable;
import fr.rhaz.ipfs.daemon.Daemon;
import fr.rhaz.ipfs.daemon.DaemonEvent;
import io.ipfs.api.IPFS;
import io.ipfs.api.KeyInfo;
import java.io.IOException;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 *
 * @author CraigDev
 */
public class DaemonManager {

    public static Daemon ipfsDaemon;

    public static IPFS ipfsObject;

    public static StringProperty daemonEventState = new SimpleStringProperty("Starting IPFS Daemon and Node!!!");

    public DaemonManager() {
        ipfsDaemon = new Daemon();
        ipfsDaemon.run(() -> init());

        // attach event manager to daemon
        ipfsDaemon.getEventManager().register(new EventRunnable<DaemonEvent>() {
            @Override
            public void execute(DaemonEvent e) {
                //broadcast DaemonEvent as needed by the application
                if (e.getType().equals(DaemonEvent.DaemonEventType.DAEMON_STARTED)) {
//                    daemonEventState.set("IPFS is starting...");
                    System.out.println("IPFS is starting...");
                }
                if (e.getType().equals(DaemonEvent.DaemonEventType.DAEMON_STOPPED)) {
//                    daemonEventState.set("IPFS is stopped");
                    System.out.println("IPFS is stopped");
                }
                if (e.getType().equals(DaemonEvent.DaemonEventType.INIT_DONE)) {
//                    daemonEventState.set("Initialization Completed. IPFS is starting...");
                    System.out.println("Initialization Completed. IPFS is starting...");
                }
                if (e.getType().equals(DaemonEvent.DaemonEventType.INIT_STARTED)) {
//                    daemonEventState.set("Initialization Started. IPFS is starting...");
                    System.out.println("Initialization Started. IPFS is starting...");
                }
                if (e.getType().equals(DaemonEvent.DaemonEventType.ATTACHED)) {
//                    daemonEventState.set("IPFS is Up and running");
                    System.out.println("IPFS is Up and running");
                    System.out.println("Start File Storage Implemntation");
                }
            }
        });
    }

    public static void init() {
        try {
            ipfsDaemon.binaries();
        } catch (IOException ex) {
            Logger.getLogger(DaemonManager.class.getName()).log(Level.SEVERE, null, ex);
        }
        ipfsDaemon.start();
        ipfsDaemon.attach();
        ipfsObject = ipfsDaemon.getIPFS();
        IPFSHandler.setIpfs(ipfsObject);
    }
    
    
    public static void doStart(){
        ipfsDaemon.start();
        ipfsDaemon.attach();
        ipfsObject = ipfsDaemon.getIPFS();
    }

    public static void stopDaemon() {
        ipfsDaemon.stop();
    }

    public static Boolean isDaemonRunning() {
        return ipfsDaemon != null;
    }

    public static IPFS getIPFS() {
        return ipfsObject;
    }

    public static KeyInfo getKeyInfo(String name) throws IOException {
        return ipfsObject.key.gen(name, Optional.of("rsa"), Optional.of("2048"));
    }

    public static void deletePeerKey(String name) {
        try {
            ipfsObject.key.rm(name);
        } catch (IOException ex) {
            Logger.getLogger(DaemonManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
