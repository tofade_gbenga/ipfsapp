package ipfsapp.utils;

import com.jfoenix.controls.JFXDrawer;
import com.jfoenix.controls.JFXHamburger;
import com.jfoenix.transitions.hamburger.HamburgerSlideCloseTransition;
import ipfsapp.DaemonManager.IPFSHandler;
import ipfsapp.DataHandler.DataDAO;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class IPFSUtil {

    private static final int BUFFER_SIZE = 4096;

    public static final String ICON_IMAGE_LOC = "/ipfsapp/resources/ipfs.png";
    private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");
    public static final ExecutorService TASK_EXECUTOR = Executors.newSingleThreadExecutor();
    public static ScheduledExecutorService TIMED_TASK_EXEC = Executors.newScheduledThreadPool(4);
    public static ExecutorService SOCKET_THREAD_POOL = Executors.newCachedThreadPool();
    public static String NO_INTERNET = "Looks like your device is not connected to the internet, please connect and try again!!!";

    public static void setStageIcon(Stage stage) {
        stage.getIcons().add(new Image(ICON_IMAGE_LOC));
    }

    public static Object loadWindow(URL loc, String title, Stage parentStage) {
        Object controller = null;
        try {
            FXMLLoader loader = new FXMLLoader(loc);
            Parent parent = loader.load();
            controller = loader.getController();
            Stage stage = null;
            if (parentStage != null) {
                stage = parentStage;
            } else {
                stage = new Stage(StageStyle.DECORATED);
            }
            stage.setTitle(title);

            stage.setScene(new Scene(parent));
            stage.show();
            setStageIcon(stage);
            stage.centerOnScreen();
        } catch (IOException ex) {
            Logger.getLogger(IPFSUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
        return controller;
    }

    public static String formatDateTimeString(Date date) {
        return DATE_TIME_FORMAT.format(date);
    }

    public void initDrawer(JFXHamburger hamburger, JFXDrawer drawer, Class cls) {
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("/ipfsapp/ui/MainView/ToolBar/toolbar.fxml"));
            VBox toolbar = loader.load();
            drawer.setSidePane(toolbar);
//            ToolbarController controller = loader.getController();
//            controller.setBookReturnCallback(this);
        } catch (IOException ex) {
            Logger.getLogger(cls.getName()).log(Level.SEVERE, null, ex);
        }
        HamburgerSlideCloseTransition task = new HamburgerSlideCloseTransition(hamburger);
        task.setRate(-1);
        hamburger.addEventHandler(MouseEvent.MOUSE_CLICKED, (Event event) -> {
            drawer.toggle();
        });
        drawer.setOnDrawerOpening((event) -> {
            task.setRate(task.getRate() * -1);
            task.play();
            drawer.toFront();
        });
        drawer.setOnDrawerClosed((event) -> {
            drawer.toBack();
            task.setRate(task.getRate() * -1);
            task.play();
        });
    }

    public static String getDateString(Date date) {
        return DATE_FORMAT.format(date);
    }

    public static void closeStage(Node node) {
        ((Stage) node.getScene().getWindow()).close();
    }

    //    method checks for internet connection
    public static boolean connectivity() {
        try {
            URL url = new URL("http://www.google.com");

            URLConnection connection = url.openConnection();
            connection.connect();

            return true;

        } catch (Exception e) {

            return false;

        }
    }

    public static List<String> convertStringToList(String text) {
        return Arrays.asList(text.split("\\r?\\n"));
    }

    public static String createStringFromList(List<String> strings) {
        StringBuilder sb = new StringBuilder();

        strings.stream().forEach(string -> sb.append(string).append("\n"));

        return sb.toString().trim();
    }

    /**
     * Get application working directory
     *
     * @return String
     */
    public static String getAppDirectory() {
        return System.getProperty("user.dir");
    }

    /**
     * Rebroadcast/ Republish Name Space to network
     */
    public static void reBroadcastNameSpace() {

        ScheduledService<Boolean> svc = new ScheduledService<Boolean>() {
            @Override
            protected Task<Boolean> createTask() {
                return new Task<Boolean>() {
                    @Override
                    protected Boolean call() throws Exception {
                        if (connectivity()) {
                            IPFSHandler ipfsh = new IPFSHandler();
                            return ipfsh.reBroadCastNameSpace();
                        }
                        return false;
                    }
                };
            }
        };

        svc.setDelay(Duration.minutes(10));
        svc.setPeriod(Duration.minutes(30));

        svc.setOnFailed(e -> System.out.println("Republishing Name Space failed"));

        svc.setOnSucceeded(e -> {
            System.out.println(svc.getValue() ? "Name Space Successfully Republished" : "Name Space Could Not Be Republished");
        });

        svc.start();
    }

    /**
     * Task to get all users hash
     */
    public static void setAllBlocks() {

        ScheduledService<Boolean> svc = new ScheduledService<Boolean>() {
            @Override
            protected Task<Boolean> createTask() {
                return new Task<Boolean>() {
                    @Override
                    protected Boolean call() throws Exception {
                        if (connectivity()) {
                            IPFSHandler handler = new IPFSHandler();
                            List<String[]> list = new ArrayList<>(handler.resolveAllPeerFiles());
                            if (!list.isEmpty()) {
                                DataDAO.truncateTable("USERBLOCKS");
                                return DataDAO.insertUserBlockBatch(list);
                            }
                        }
                        return false;
                    }
                };
            }
        };

        svc.setDelay(Duration.minutes(2));
        svc.setPeriod(Duration.minutes(10));

        svc.setOnFailed(e -> System.out.println("Setting all user blocks failed"));

        svc.setOnSucceeded(e -> {
            System.out.println(svc.getValue() ? "All Users Blocks Successfully set" : "All Users Blocks could not be set");
        });

        svc.start();
    }

    /**
     * Publish my block
     */
    public static void publishMyBlocks() {

        ScheduledService<Boolean> svc = new ScheduledService<Boolean>() {
            @Override
            protected Task<Boolean> createTask() {
                return new Task<Boolean>() {
                    @Override
                    protected Boolean call() throws Exception {
                        if (connectivity()) {
                            IPFSHandler ipfsh = new IPFSHandler();
                            return ipfsh.rebroadcastHashes();
                        }
                        return false;
                    }
                };
            }
        };

        svc.setDelay(Duration.minutes(1));
        svc.setPeriod(Duration.minutes(10));

        svc.setOnSucceeded(e -> {
            System.out.println(svc.getValue() ? "Your Blocks were successfully broadcasted" : "You Blocks could not be broadcasted");
        });

        svc.setOnFailed(e -> System.out.println("Publishing my blocks failed"));
        svc.start();
    }

    public static void unzip(String zipFilePath, File destDirectory) throws IOException {
//        File destDir = new File(destDirectory);
        if (!destDirectory.exists()) {
            destDirectory.mkdir();
        }
        try (ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath))) {
            ZipEntry entry = zipIn.getNextEntry();
            // iterates over entries in the zip file
            while (entry != null) {
                String filePath = destDirectory + File.separator + entry.getName();
                if (!entry.isDirectory()) {

                    // if the entry is a file, extracts it
                    extractFile(zipIn, filePath);
                } else {
                    // if the entry is a directory, make the directory
                    File dir = new File(filePath);
                    dir.mkdir();
                }
                zipIn.closeEntry();
                entry = zipIn.getNextEntry();
            }
        } catch (IOException ex) {
            Logger.getLogger(IPFSUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private static void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
        BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
        byte[] bytesIn = new byte[BUFFER_SIZE];
        int read = 0;
        while ((read = zipIn.read(bytesIn)) != -1) {
            bos.write(bytesIn, 0, read);
        }
        bos.close();
    }
}
